import {GET_LIST_TRANSACTION, GET_LIST_TRANSACTION_SUCCESS, GET_LIST_TRANSACTION_ERROR} from '../action/index';

const initialState = {
    pending: false,
    transactions: [],
    error: null
}

export default function transactionReducer(state = initialState, action) {
    switch(action.type) {
        case GET_LIST_TRANSACTION: 
            return {
                ...state,
                pending: true
            }
        case GET_LIST_TRANSACTION_SUCCESS:
            return {
                ...state,
                pending: false,
                transactions: action
            }
        case GET_LIST_TRANSACTION_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: 
            return state;
    }
}

export const getTransactions = state => state.transactions;
export const getTransactionsPending = state => state.pending;
export const getTransactionsError = state => state.error;