import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import "./App.css";
import Home from "./route/home";
import Detail from "./route/detail";

function App(){
  return(
    <Router>
    <Switch>
     <Route exact path="/" component={Home} />
     <Route path="/detail" component={Detail} />
   </Switch>
 </Router>
  )
}

export default App;
