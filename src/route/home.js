import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Web3 from "web3";
import Web3Modal from "web3modal";
import { fetchTransactions } from "./../action/index";
import {get} from "lodash";

function Home() {
  const dispatch = useDispatch();

  const [account, setAccount] = useState(null);
  const [balance, setBalance] = useState();
  const [isconnected, setIsConnected] = useState(false);
  const APIKey = "THHBEXG4F7A742U6BNNV6KUAA89KCTCU2M";

  const data = useSelector((state) => state.transactionReducer);
  const objtransaction = data.transactions;
  const { transactions } = objtransaction;
  const result = get(transactions,"result");

  useEffect(() => {
    if (account) {
      dispatch(fetchTransactions({ account , APIKey }));
    }
  }, [account]);


  const providerOptions = {};
  const web3Modal = new Web3Modal({
    network: "mainnet", // optional
    cacheProvider: true, // optional
    providerOptions, // required
  });

  async function connect() {
    try {
      const testnet = "https://bsc-dataseed.binance.org";
      const provider = await web3Modal.connect();
      let web3 = new Web3(provider);
      const accounts = await web3.eth.getAccounts();
      web3 = new Web3(new Web3.providers.HttpProvider(testnet));
      const balance = await web3.eth.getBalance(accounts[0]);
      setAccount(accounts[0]);
      setBalance(balance);
      setIsConnected(true);
    } catch (err) {
      console.error(err);
    }
  }

  async function disconnect() {
    setIsConnected(false);
    setAccount(null);
    setBalance();
    await web3Modal.clearCachedProvider();
  }

  return (
    <div className="App">
      <div className="container">
        <div className="connect">
          <div className="balance">
            <span>Your balance: </span>
            {balance ? balance / 1000000000000000000 : "0"} BNB
          </div>
          <div className="address">
            <span>Adress wallet: </span>
            {account}
          </div>
          {!isconnected ? (
            <button className="btn__connect" onClick={connect}>
              Connect to metamask
            </button>
          ) : (
            <button className="btn__connect disconnect" onClick={disconnect}>
              Disconnect
            </button>
          )}
        </div>
        <div className="list__transaction">
          <h4>List Transaction</h4>
          <div className="row-transaction">
            <table>
            <thead>
              <tr>
                <th>BlockNumber</th>
                <th>Gas</th>
                <th>From</th>
                <th>To</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
             {result&&account? result.map((item,index) =>{
               return (
                <tr key={index}>
                <td>{item.blockNumber}</td>
                <td>{item.gas}</td>
                <td>{item.from}</td>
                <td>{item.to}</td>
                <td>{item.txreceipt_status==1?<button className="btn__succeed">Succeed</button>:<button className="btn__fail">Fail</button>}</td>
              </tr>
               )
             }):[]}
               </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;