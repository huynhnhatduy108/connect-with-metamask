export const GET_LIST_TRANSACTION = "GET_LIST_TRANSACTION";
export const GET_LIST_TRANSACTION_SUCCESS = "GET_LIST_TRANSACTION_SUCCESS";
export const GET_LIST_TRANSACTION_ERROR = "GET_LIST_TRANSACTION_ERROR";

export function fetchListTransactions() {
  return {
    type: GET_LIST_TRANSACTION,
  };
}

export function fetchListTransactionsSuccess(transactions) {
  return {
    type: GET_LIST_TRANSACTION_SUCCESS,
    transactions,
  };
}

export function fetchListTransactionsError(error) {
  return {
    type: GET_LIST_TRANSACTION_SUCCESS,
    error,
  };
}
export function fetchTransactions({ account, APIKey }) {
  const endpointListTransactions = `https://api.bscscan.com/api?module=account&action=txlist&address=${account}&startblock=1&endblock=99999999&sort=asc&apikey=${APIKey}`;
  return (dispatch) => {
    dispatch(fetchListTransactions());
    fetch(endpointListTransactions)
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          throw res.error;
        }
        console.log("data",res);
        dispatch(fetchListTransactionsSuccess(res));
        return res;
      })
      .catch((error) => {
        dispatch(fetchListTransactionsError(error));
      });
  };
}

// export default fetchTransactions;
